import React from "react";
import Waifu from "./waifu";
import PropTypes from "prop-types" 

class Waifus extends React.Component{

    render(){
        return this.props.waifus.map((e) => <Waifu waifu ={e} key={`${e.name}component`} deleteWaifu={this.props.deleteWaifu}></Waifu>
          )      
    }
}

Waifus.propTypes ={
    waifus : PropTypes.array.isRequired
}
export default Waifus;