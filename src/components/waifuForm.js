import React from "react";


class WaifuForm extends React.Component {

    state = {
        styleInput: { color: "#000000", backgroundColor: '#b83abd', fontSize: '30px', margin: "0px 0px 0px 650px" },
        styleH3: { color: "#b83abd", fontSize: '30px', textShadow: " 0 1px 0px #000000, 1px 0 0px #000000, 1px 2px 1px #000000, 2px 1px 1px #000000, 2px 3px 2px #000000, 3px 2px 2px #000000, 3px 4px 2px #000000, 4px 3px 3px #000000, 4px 5px 3px #000000, 5px 4px 2px #000000, 5px 6px 2px #000000, 6px 5px 2px #000000, 6px 7px 1px #000000, 7px 6px 1px #000000, 7px 8px 0px #000000, 8px 7px 0px #000000", margin: "0px 0px 0px 650px" },
        styleButton: { color: "#000000", backgroundColor: '#b83abd', padding: "10px", fontSize: '25px', margin: "0px 0px 0px 770px", borderRadius: "10%", border: "5px solid #000000", cursor: "pointer" },
        name: '',
        city: '',
        type: '',
        color: '',
        background: '',
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.addWaifu({ name: this.state.name, city: this.state.city, type: this.state.type, color: this.state.color, background: this.state.background });
        this.setState({ name: '', city: '', type: '', color: '', background: '' })

    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <form style={{ border: "2px solid #000000", padding: "30px" }} onSubmit={this.onSubmit}>
                <h3 style={this.state.styleH3}>CREATE YOUR WAIFU</h3>
                <br />
                <h3 style={this.state.styleH3}>What is her name? </h3>
                <br />
                <input style={this.state.styleInput} type='text' onChange={this.onChange} value={this.state.name} name="name" />
                <h3 style={this.state.styleH3} >Where she lives?</h3>
                <br />
                <input style={this.state.styleInput} type='text' onChange={this.onChange} value={this.state.city} name="city" />
                <h3 style={this.state.styleH3}>What is she?</h3>
                <br />
                <input style={this.state.styleInput} type='text' onChange={this.onChange} value={this.state.type} name="type" />
                <br />
                <h3 style={this.state.styleH3}>Select a color for you waifu (Letters)</h3>
                <br />
                <input style={this.state.styleInput} type='text' onChange={this.onChange} value={this.state.color} name="color" />
                <br />
                <h3 style={this.state.styleH3}>Select a color for you waifu (Background)</h3>
                <br />
                <input style={this.state.styleInput} type='text' onChange={this.onChange} value={this.state.background} name="background" />
                <br />
                <br />
                <input style={this.state.styleButton} type='submit' value='SAVE' />
            </form>
        )
    }
}

export default WaifuForm;