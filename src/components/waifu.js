import React from "react";
import PropTypes from "prop-types"
class Waifu extends React.Component {

    state = {
        styleButton: {}
    }

    addStyleDeleteButton = (waifu) => {
        return { fontSize: '40px', backgroundColor: waifu.backgroundColor, border: waifu.border, color: waifu.color }
    }

    render() {
        const { waifu } = this.props;
        return <div style={waifu.style} key={`div${waifu.name}`}>
            <h1 key={`name${waifu.name}`}>{waifu.name}</h1>
            <h1 key={`type${waifu.name}`}>{waifu.type}</h1>
            <h1 key={`city${waifu.name}`}>{waifu.city}</h1>
            <button key={`deleteButton${waifu.name}`} style={this.addStyleDeleteButton(waifu.style)} onClick={this.props.deleteWaifu.bind(this,waifu.name )}>DELETE</button>
        </div>
    }
}


Waifu.propTypes = {
    waifu: PropTypes.object.isRequired
}
export default Waifu;