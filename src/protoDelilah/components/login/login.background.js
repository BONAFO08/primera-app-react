import React from "react";
import urlImgs from "./login.background.imgs.json"
import './login.css'
class LoginBackground extends React.Component {

    selectImgBackground = ()=>{
        return Math.floor(Math.random() * urlImgs.imgs.length);
    }

    render() {
        return <div key={"login-background"}  
        className="login-background" 
        style={{backgroundImage : `url("${urlImgs.imgs[this.selectImgBackground()]}")`}}>
        </div>
    }
}


export default LoginBackground;