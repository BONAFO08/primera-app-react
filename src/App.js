import React from 'react';
import waifus from './database/data.json'
import Waifus from './components/waifus';
import WaifuForm from './components/waifuForm';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';

//PROTO DELILAH
import LoginBackground from './protoDelilah/components/login/login.background';
import LoginIndex from './protoDelilah/components/login/login.index';
import 'bootstrap/dist/css/bootstrap.min.css'

class App extends React.Component {
  state = {
    waifus: JSON.parse(sessionStorage.getItem('waifu')) || waifus
  }

  addWaifu = (waifu) => {
    const newWaifu = {
      id: this.state.waifus.length + 1,
      name: waifu.name,
      type: waifu.type,
      city: waifu.city,
      style: { backgroundColor: `#${waifu.background}`, border: "2px solid #000000", color: `#${waifu.color}` },
    }
    this.setState({
      waifus: [...this.state.waifus, newWaifu]
    })

  }

  deleteWaifu = (name) => {
    const updatedWaifus = this.state.waifus.filter(waifu => waifu.name.toLowerCase() !== name.toLowerCase())
    this.setState({
      waifus: updatedWaifus
    })
  }

  render() {
    sessionStorage.setItem('waifu', JSON.stringify(this.state.waifus))
    return (
      <div >
        <Router>
          <Routes>
            <Route exact path='/waifus' element={<div><Waifus waifus={this.state.waifus} deleteWaifu={this.deleteWaifu}></Waifus></div>}>
            </Route>
            <Route exact path='/create' element={<div><WaifuForm addWaifu={this.addWaifu}></WaifuForm></div>}>
            </Route>
            <Route exact path='/delilah' element={<div>
              <LoginBackground></LoginBackground>
              <LoginIndex></LoginIndex>
              </div>}>
            </Route>
          </Routes>
        </Router>
      </div>
    )
  }
}


export default App;
